const { query } = require('express');
const Model = require('../model');
const { Replies, Questions } = Model;

const fs = require('fs');

const repliesController = {
  // 获取回答
  async get_reply(req, res) {
    var qs;
    var mquery_post = Questions.find({
      _id: req.query['question_id'],
    });
    mquery_post.exec((err, question) => {
      if (err) {
        res.status(400).json({
          code: 400,
          msg: '查询失败',
          err: err,
        });
      } else {
        question.forEach(element => {
          let x = false;
          element['liked'].forEach(like => {
            if (req.query['user_id'] == like) {
              x = true;
            }
          });
          qs = {
            question_id: element['_id'],
            question_head: element['head'],
            question_content: element['content'],
            user_id: element['user_id'],
            reply_timestamp: element['reply_stamp'],
            reply_count: element['reply_cnt'],
            like_count: element['liked'].length,
            like_state: x,
            stick_state: element['sticked'],
            image_list: element['image_list'],
          };
        });
      }
    });

    var mquery = Replies.find({
      question_id: req.query['question_id']
    });
    mquery.sort({ reply_stamp: -1 });
    var mquery_count=mquery.clone();
    const counter = await mquery_count.count();
    mquery.skip(req.query['idx_up']);
    mquery.limit(req.query['idx_low'] - req.query['idx_up']);
    mquery.exec((err, reply) => {
      if (err) {
        res.status(400).json({
          code: 400,
          msg: '查询失败',
          err: err,
        });
      } else {
        reply_list = [];
        reply.forEach(element => {
          let x = false;
          element['liked'].forEach(like => {
            if (req.query['user_id'] == like) {
              x = true;
            }
          });
          let rpl = {
            reply_id: element['_id'],
            reply_content: element['reply_content'],
            user_id: element['user_id'],
            reply_timestamp: element['reply_stamp'],
            like_count: element['liked'].length,
            like_state: x,
          };
          reply_list.push(rpl);
        });
        res.json({
          code: 200,
          msg: '查询成功',
          question: qs,
          reply_count: counter,
          list_len: reply.length,
          reply_list: reply_list,
        });
      }
    });
  },
  // 添加回答
  add_reply(req, res) {
    time_now = Date.parse(new Date());
    var reply = new Replies({
      question_id: req.body['question_id'],
      reply_content: req.body['reply_content'],
      user_id: req.body['user_id'],
      reply_stamp: time_now,
      liked: [],
    });
    reply.save((err) => {
      if (err) {
        res.status(400).json({
          code: 400,
          msg: '添加失败',
          err: err,
        });
      } else {
        var mquery = Questions.find({
          _id: req.body['question_id']
        });
        mquery.update({
          $set: {
            reply_stamp: time_now
          },
          $inc: {
            reply_cnt: 1
          }
        });
        mquery.exec((err) => {
          if (err) {
            res.status(400).json({
              code: 400,
              msg: '修改失败',
              err: err,
            });
          } else {
            res.json({
              code: 200,
              msg: '添加成功',
              reply_id: reply['_id']
            });
          }
        });
      }
    });
  },
  // 删除回答
  delete_reply(req, res) {
    Replies.findOneAndDelete({ _id: req.body['reply_id'] }, (err, reply) => {
      if (err) {
        res.status(400).json({
          code: 400,
          msg: '删除失败',
          err: err,
        });
      } else {
        reply.image_list.forEach(element => {
          fs.unlink('/images/'+element);
        });
        var mquery = Questions.find({
          _id: reply.question_id
        });
        mquery.update({
          $inc: {
            reply_cnt: -1
          }
        });
        mquery.exec((err) => {
          if (err) {
            res.status(400).json({
              code: 400,
              msg: '删除失败',
              err: err,
            });
          } else {
            res.json({
              code: 200,
              msg: '删除成功',
            });
          }
        });
      }
    });
  },
  // 添加点赞
  like_reply(req, res) {
    var mquery = Replies.find({
      _id: req.body['reply_id']
    });
    if (Boolean(parseInt(req.body['like_type'])) == true) {
      mquery.update({
        $push: {
          liked: req.body['user_id']
        }
      });
    } else {
      mquery.update({
        $pull: {
          liked: req.body['user_id']
        }
      });
    }
    mquery.exec((err, reply) => {
      if (err) {
        res.status(400).json({
          code: 400,
          msg: '修改失败',
          err: err,
        });
      } else {
        res.json({
          code: 200,
          like_state: Boolean(parseInt(req.body['like_type'])),
          msg: '修改成功'
        });
      }
    });
  }
};

module.exports = repliesController;