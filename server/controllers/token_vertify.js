const jwt = require('jsonwebtoken');
const signkey = 'raft_keyboard_cat'; // 课程项目，先写死

exports.setToken = (email) => {
  return new Promise((resolve, reject) => {
    jwt.sign({ email }, signkey, { expiresIn: '1h' }, (err, token) => {
      if (err) {
        reject(err);
      } else {
        resolve(token);
      }
    });
  });
};

exports.verifyToken = (token) => {
  return new Promise((resolve, reject) => {
    jwt.verify(token, signkey, (err, decoded) => {
      if (err) {
        reject(err);
      } else {
        resolve(decoded);
      }
    });
  });
};