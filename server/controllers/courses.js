const { query } = require('express');
const Model = require('../model');
const { Courses } = Model;

const coursesController = {
  // 获取课程
  get_courses(req, res) {
    var mquery = Courses.find({});
    let andall = [];
    if (req.query['_id']) {
      let regs = [];
      if (Array.isArray(req.query['_id'])) {
        req.query['_id'].forEach((item) => {
          regs.push({
            _id: {
              $regex: item,
              $options: 'i'
            }
          });
        });
        andall.push({
          $or: regs
        });
      }
      else {
        regs = new RegExp(req.query['_id'], 'i');
        mquery.where('_id').regex(regs);
      }
    }
    if (req.query['name']) {
      let regs = [];
      if (Array.isArray(req.query['name'])) {
        req.query['name'].forEach((item) => {
          regs.push({
            name: {
              $regex: item,
              $options: 'i'
            }
          });
        });
        andall.push({
          $or: regs
        });
      }
      else {
        regs = new RegExp(req.query['name'], 'i');
        mquery.where('name').regex(regs);
      }
    }
    if (req.query['teacher']) {
      let regs = [];
      if (Array.isArray(req.query['teacher'])) {
        req.query['teacher'].forEach((item) => {
          regs.push({
            teacher: {
              $regex: item,
              $options: 'i'
            }
          });
        });
        andall.push({
          $or: regs
        });
      }
      else {
        regs = new RegExp(req.query['teacher'], 'i');
        mquery.where('teacher').regex(regs);
      }
    }
    if (req.query['tid']) {
      let regs = [];
      if (Array.isArray(req.query['tid'])) {
        req.query['tid'].forEach((item) => {
          regs.push({
            tid: {
              $regex: item,
              $options: 'i'
            }
          });
        });
        andall.push({
          $or: regs
        });
      }
      else {
        regs = new RegExp(req.query['tid'], 'i');
        mquery.where('tid').regex(regs);
      }
    }
    if (req.query['place']) {
      let regs = [];
      if (Array.isArray(req.query['place'])) {
        req.query['place'].forEach((item) => {
          regs.push({
            place: {
              $regex: item,
              $options: 'i'
            }
          });
        });
        andall.push({
          $or: regs
        });
      }
      else {
        regs = new RegExp(req.query['place'], 'i');
        mquery.where('place').regex(regs);
      }
    }
    if (req.query['type']) {
      let regs = [];
      if (Array.isArray(req.query['type'])) {
        req.query['type'].forEach((item) => {
          regs.push({
            type: {
              $regex: item,
              $options: 'i'
            }
          });
        });
        andall.push({
          $or: regs
        });
      }
      else {
        regs = new RegExp(req.query['type'], 'i');
        mquery.where('type').regex(regs);
      }
    }
    if (req.query['language']) {
      let regs = [];
      if (Array.isArray(req.query['language'])) {
        req.query['language'].forEach((item) => {
          regs.push({
            language: {
              $regex: item,
              $options: 'i'
            }
          });
        });
        andall.push({
          $or: regs
        });
      }
      else {
        regs = new RegExp(req.query['language'], 'i');
        mquery.where('language').regex(regs);
      }
    }
    if (req.query['department']) {
      let regs = [];
      if (Array.isArray(req.query['department'])) {
        req.query['department'].forEach((item) => {
          regs.push({
            department: {
              $regex: item,
              $options: 'i'
            }
          });
        });
        andall.push({
          $or: regs
        });
      }
      else {
        regs = new RegExp(req.query['department'], 'i');
        mquery.where('department').regex(regs);
      }
    }
    if (req.query['room']) {
      let regs = [];
      if (Array.isArray(req.query['room'])) {
        req.query['room'].forEach((item) => {
          regs.push({
            room: {
              $regex: item,
              $options: 'i'
            }
          });
        });
        andall.push({
          $or: regs
        });
      }
      else {
        regs = new RegExp(req.query['room'], 'i');
        mquery.where('room').regex(regs);
      }
    }
    // if (req.query['period']) {
    //   if (Array.isArray(req.query['period'])) {
    //     let tmp = req.query['period'].map(Number);
    //     mquery.where('period').in(tmp);
    //   }
    //   else {
    //     mquery.where('period').equals(req.query['period']);
    //   }
    // }
    // if (req.query['day']) {
    //   if (Array.isArray(req.query['day'])) {
    //     let tmp = req.query['day'].map(Number);
    //     mquery.where('day').in(tmp);
    //   }
    //   else {
    //     mquery.where('day').equals(req.query['day']);
    //   }
    // }
    if (req.query['day_period']) {
      let regs = [];
      test = req.query['day_period'];
      req.query['day_period'].forEach((item) => {
        tmp = item.replace('[', '').replace(']', '').replaceAll('\\', '').replaceAll('"', '').split(',');
        tmp = tmp.map(Number);
        let day = Number(tmp[0]);
        let period = Number(tmp[1]);
        test = period;
        if (period) {
          regs.push({
            day: day,
            period: period
          });
        } else {
          regs.push({
            day: day
          });
        }
      });
      andall.push({
        $or: regs
      });
    }
    if (andall.length > 0) {
      mquery.and(andall);
    }
    mquery.exec((err, courses) => {
      if (err) {
        res.status(400).json({
          code: 400,
          msg: '查询失败',
          err: err,
        });
      } else {
        res.json({
          code: 200,
          num: courses.length,
          courses: courses,
        });
      }
    });
  },
  // 添加课程
  add_courses(req, res) {
    var course = new Courses({
      _id: req.body['_id'],
      name: req.body['name'],
      teacher: req.body['teacher'],
      tid: req.body['tid'],
      place: req.body['place'],
      type: req.body['type'],
      language: req.body['language'],
      department: req.body['department'],
    });
    course.save((err) => {
      if (err) {
        res.status(400).json({
          code: 400,
          msg: '添加失败',
          err: err,
        });
      } else {
        res.json({
          code: 200,
          msg: '添加成功',
        });
      }
    });
  },
};

module.exports = coursesController;
