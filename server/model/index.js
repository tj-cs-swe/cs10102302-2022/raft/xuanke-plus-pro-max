const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const model = mongoose.model.bind(mongoose);
const ObjectId = Schema.Types.ObjectId;


const userSchema = new Schema({
  account: String,
  password: String,
  name: String,
  role: String,
  gender: String,
  year: String,
  date: String,
  grade: String,
  region: String,
  email: String,
  desc: String,
  courses: Array,
  favorites_courses: Array,
  avatar: String
});

const courseSchema = new Schema({
  _id: String,
  name: String,
  teacher: String,
  tid: String,
  place: String,
  type: String,
  language: String,
  department: String,
  length: Number,
  period: Number,
  day: Number,
  room: String,
});

const courseinfoSchema = new Schema({
  course_id: String,
  msg: String,
  courseTitle: String,
  courseTime: String,
  credit: String,
  teacher: String,
  assessment: String,
  teacher_message: String,
  course_intro: String,
  outline: Array,
  labels: Array,
});

const commentsSchema = new Schema({
  course_id: String,
  score: Number,
  user_id: String,
  content: String,
  time: Number,
  liked: Array,
  like_count: Number,
  pic: String,
});

const emailSchema = new Schema({
  email: String,
  code: String,
  time: Date,
});

const questionSchema = new Schema({
  course_id: String,
  head: String,
  content: String,
  thumbnail: String,
  user_id: String,
  timestamp: Number,
  reply_stamp: Number,
  reply_cnt: Number,
  liked: Array,
  sticked: Boolean,
  image_list: Array,
});


const replySchema = new Schema({
  question_id: String,
  reply_content: String,
  user_id: String,
  reply_stamp: Number,
  liked: Array,
  image_list: Array,
});

const Users = model('Users', userSchema);
const Emails = model('Emails', emailSchema);
const Courses = model('Courses', courseSchema);
const Courseinfos = model('Courseinfos', courseinfoSchema);
const Comments = model('Comments', commentsSchema);
const Questions = model('Questions', questionSchema);
const Replies = model('Replies', replySchema);

module.exports = { Users, Emails, Courses, Courseinfos, Comments, Questions, Replies };