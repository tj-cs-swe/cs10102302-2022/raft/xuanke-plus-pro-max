# 随机生成课程信息
import json
import random
# 文件路径
input = 'courses.json'
output = 'courses_ad.json'
if __name__ == "__main__":
    f_in = open(input, encoding='utf-8') 
    re = json.load(f_in)
    for i in re:
      i["day"] = random.randint(1, 5)
      i["length"] = 2
      i["period"] = random.randrange(1, 9, 2)
      roomint = random.randint(1, 4) *100 + random.randint(1, 15)
      room = random.choice(["A", "B", "F", "G"]) + str(roomint)
      i["room"] = room
    ans = json.dumps(re,ensure_ascii=False)
    f_out = open(output, 'w', encoding='utf-8') 
    f_out.write(ans)